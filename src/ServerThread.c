/*
 * ServerThread.c
 *
 *  Created on: 4 ���. 2020 �.
 *      Author: mesh
 */


#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "xgpio.h"
#include "xiicps.h"
#include "lwip/sockets.h"
#include "netif/xadapter.h"
#include "lwipopts.h"
#include "xil_printf.h"
#include "FreeRTOS.h"
#include "task.h"
#include "ServerThread.h"
#include "FreeRTOS_CLI.h"

#define THREAD_STACKSIZE 1024

/* Dimensions the buffer into which input characters are placed. */
#define cmdMAX_INPUT_SIZE	100

/* Dimensions the buffer into which string outputs can be placed. */
#define cmdMAX_OUTPUT_SIZE	1024

const char *pcWelcomeMessage = "FreeRTOS command server - connection accepted.\r\nType Help to view a list of registered commands.\r\n\r\n>";


static BaseType_t prvPingCommand(int8_t *pcWriteBuffer, size_t xWriteBufferLen, const int8_t *pcCommandString) {
int8_t *pcParameter1;
BaseType_t xParameter1StringLength, xResult;

	pcParameter1 = FreeRTOS_CLIGetParameter(pcCommandString, 1, &xParameter1StringLength);
	pcParameter1[xParameter1StringLength] = 0x00;

	if (pcParameter1 == NULL) {
		xil_printf("Parameter1 empty\n");
		snprintf(pcWriteBuffer, xWriteBufferLen, "pong");
	} else {
		xil_printf("Parameter1: %s\n", pcParameter1);
		snprintf(pcWriteBuffer, xWriteBufferLen, "pong %s", pcParameter1);
	}
	vTaskDelay(1000);
	return pdFALSE;
}


static const CLI_Command_Definition_t xPingCommand = {
		"ping",
		"ping <arg>: Reply arg to client",
		prvPingCommand,
		1
};


void ServerWorker(void *pvParameters) {
int sock = (int)pvParameters;
char cInputString[ cmdMAX_INPUT_SIZE ], cOutputString[ cmdMAX_OUTPUT_SIZE ];
int lInputIndex, lBytes;
portBASE_TYPE xReturned;

TaskHandle_t handle = xTaskGetCurrentTaskHandle();
	xil_printf("Worker Thread: 0x%08X\n", handle);

	lwip_send(sock, pcWelcomeMessage, strlen(pcWelcomeMessage), 0);

	lInputIndex = 0;
	memset(cInputString, 0x00, cmdMAX_INPUT_SIZE);

	char cInChar;
	do {
		lBytes = lwip_recv(sock, &cInChar, sizeof(cInChar), 0);
		if (lBytes > 0) {

			if (cInChar == '\n') {
				if (strcmp("quit", (const char *)cInputString) == 0) {
					lBytes = 0; // ���������� ��� ������ � �������� ����������
				} else {
					do {
						xReturned = FreeRTOS_CLIProcessCommand(cInputString, cOutputString, cmdMAX_INPUT_SIZE);
						lwip_send(sock, cOutputString, strlen(cOutputString), 0);
					} while (xReturned != pdFALSE);

					lInputIndex = 0;
					memset(cInputString, 0x00, cmdMAX_INPUT_SIZE);
					lwip_send(sock, "\r\n>", strlen("\r\n>"), 0);
				}
			} else {
				if (cInChar == '\r') {
					// Ignore
				} else if (cInChar == '\b') {
					if (lInputIndex > 0) {
						// Backspace
						lInputIndex--;
						cInputString[lInputIndex] = '\0';
					}
				} else {
					// ��� ��������� �������
					if (lInputIndex < cmdMAX_INPUT_SIZE) {
						cInputString[lInputIndex] = cInChar;
						lInputIndex++;
					}
				}
			}
		}
	} while (lBytes > 0);

	xil_printf("Closing socket\n");
//cleanup:
	lwip_close(sock);
//	vTaskDelete(NULL);
}


void ServerThread(void *pvParameters) {
(void)pvParameters;

int sock, new_sd;
int size;
struct sockaddr_in address, remote;

	FreeRTOS_CLIRegisterCommand(&xPingCommand);

	memset(&address, 0, sizeof(address));
	if ((sock = lwip_socket(AF_INET, SOCK_STREAM, 0)) < 0) {
		return;
	}

	address.sin_family = AF_INET;
	address.sin_port = htons(8085);
	address.sin_addr.s_addr = INADDR_ANY;

	if (lwip_bind(sock, (struct sockaddr *)&address, sizeof(address)) < 0) {
		return;
	}

	lwip_listen(sock, 0);
	size = sizeof(remote);

	for(;;) {
		new_sd = lwip_accept(sock, (struct sockaddr *)&remote, (socklen_t *)&size);
		if (new_sd > 0) {
			xil_printf("New connection accept\n");
			ServerWorker((void *)new_sd);
		}
	}
}


void StartServerThread() {
	sys_thread_new("serverd", ServerThread, 0, 1024, DEFAULT_THREAD_PRIO);
}
