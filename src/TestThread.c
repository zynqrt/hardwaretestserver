/*
 * TestThread.c
 *
 *  Created on: 31 ���. 2020 �.
 *      Author: mesh
 */
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#include "xgpio.h"
#include "xiicps.h"
#include "lwip/sockets.h"
#include "netif/xadapter.h"
#include "lwipopts.h"
#include "xil_printf.h"
#include "FreeRTOS.h"
#include "task.h"
#include "TestThread.h"


#define GPIO_OUTPUT_DEVICE_ID 		XPAR_AXI_GPO_LS_DEVICE_ID
#define GPIO_INPUT_DEVICE_ID 		XPAR_AXI_GPI_SW_DEVICE_ID

XGpio GpioOutput;


#define DACIIC_DEVICE_ID 		0
#define DACIIC_DEVICE_ADDR 		0x0F


typedef enum DACRegister_ {
	REG_DAC_A,
	REG_DAC_B,
	REG_DAC_BOTH
} DACRegister;


XIicPs IicInstance;

#define IIC_SCLK_RATE		100000
#define SLV_MON_LOOP_COUNT 0x000FFFFF	/**< Slave Monitor Loop Count*/

static s32 FindDAC(XIicPs *Instance, u16 DacAddress);
static s32 EnableReference(XIicPs *Instance, u16 DacAddress, bool Enable);
static s32 SetDACRegister(XIicPs *Instance, u16 DacAddress, DACRegister reg, u16 regdata);
static s32 EnablePowerDown(XIicPs *Instance, u16 DacAddress, bool Enable);


void test_thread(void *p) {
	s32 Status;

	Status = XGpio_Initialize(&GpioOutput, GPIO_OUTPUT_DEVICE_ID);
	if (Status != XST_SUCCESS) {
		xil_printf("Cannot init GPIO\n");
		vTaskDelay(portMAX_DELAY);
	}

	// All output
	XGpio_SetDataDirection(&GpioOutput, 1, 0);

XIicPs_Config *ConfigPtr;

	ConfigPtr = XIicPs_LookupConfig(DACIIC_DEVICE_ID);
	if (ConfigPtr == NULL) {
		xil_printf("Cannot find IIC\n");
		vTaskDelay(portMAX_DELAY);
	}

	Status = XIicPs_CfgInitialize(&IicInstance, ConfigPtr, ConfigPtr->BaseAddress);
	if (Status != XST_SUCCESS) {
		xil_printf("Cannot Init IIC\n");
		vTaskDelay(portMAX_DELAY);
	}

	XIicPs_SetSClk(&IicInstance, IIC_SCLK_RATE);

	xil_printf("Iic init OK\n");

	for(;;) {
		if (FindDAC(&IicInstance, DACIIC_DEVICE_ADDR) == XST_SUCCESS) {
//			xil_printf("DAC Found\n");

//			xil_printf("Setting Reference ON\n");
			EnablePowerDown(&IicInstance, DACIIC_DEVICE_ADDR, true);

			Status = EnableReference(&IicInstance, DACIIC_DEVICE_ADDR, true);
			if (Status != XST_SUCCESS) {
				xil_printf("Cannot enable Rederence\n");
			}
			vTaskDelay(1000);

			EnablePowerDown(&IicInstance, DACIIC_DEVICE_ADDR, false);

			Status = SetDACRegister(&IicInstance, DACIIC_DEVICE_ADDR, REG_DAC_A, 0);
			if (Status != XST_SUCCESS) {
				xil_printf("Cannot write DAC Register\n");
			}
			vTaskDelay(100);

			for (int i = 0; i < 1793; i += 256) {
//				xil_printf("Set %d to both DAC\n", i);
				Status = SetDACRegister(&IicInstance, DACIIC_DEVICE_ADDR, REG_DAC_BOTH, i);
				if (Status != XST_SUCCESS) {
					xil_printf("Cannot set %d to DAC\n", i);
				}

//				XGpio_DiscreteWrite(&GpioOutput, 1, 0b00); // Red
//				vTaskDelay(1000);

				XGpio_DiscreteWrite(&GpioOutput, 1, 0b01); // Green
				vTaskDelay(1000);

//				XGpio_DiscreteWrite(&GpioOutput, 1, 0b10); // Red
//				vTaskDelay(1000);

//				XGpio_DiscreteWrite(&GpioOutput, 1, 0b11); // Blue
//				vTaskDelay(1000);
			}

			Status = SetDACRegister(&IicInstance, DACIIC_DEVICE_ADDR, REG_DAC_BOTH, 0);
			if (Status != XST_SUCCESS) {
				xil_printf("Cannot set %d to DAC\n", 0);
			}
			vTaskDelay(100);

//			xil_printf("Setting Reference OFF\n");
			Status = EnableReference(&IicInstance, DACIIC_DEVICE_ADDR, false);
			if (Status != XST_SUCCESS) {
				xil_printf("Cannot disable Reference\n");
			}
			vTaskDelay(5000);
		}
		vTaskDelay(1000);
	}
}


static s32 SetDACRegister(XIicPs *Instance, u16 DacAddress, DACRegister reg, u16 regdata) {
u8 data[3] = {0, 0, 0};
u8 dacadr = 0;
s32 Status;

	data[0] = 0x00;

	switch (reg) {
	case REG_DAC_A:
		dacadr = 0b000;
		break;

	case REG_DAC_B:
		dacadr = 0b001;
		break;

	case REG_DAC_BOTH:
		dacadr = 0b111;
		break;

	default:
		xil_printf("Error in DACRegister\n");
		vTaskDelay(portMAX_DELAY);
	}

	u8 cmd = 0b011;

	regdata <<= 4;
	data[0] = (cmd << 3) | dacadr;
	data[1] = (regdata >> 8);
	data[2] = regdata & 0xFF;

	Status = XIicPs_MasterSendPolled(Instance, data, sizeof(data), DacAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	return XST_SUCCESS;
}


static s32 EnablePowerDown(XIicPs *Instance, u16 DacAddress, bool Enable) {
u8 data[3];
s32 Status;

	data[0] = 0b00100000;
	data[1] = 0x0;
	data[2] = (((Enable == true) ? 0b01 : 0b00) << 4 ) | 0b11;
	Status = XIicPs_MasterSendPolled(Instance, data, sizeof(data), DacAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}


static s32 EnableReference(XIicPs *Instance, u16 DacAddress, bool Enable) {
u8 data[3];
s32 Status;

	data[0] = 0b00111000;
	data[1] = 0; // Don't care
	data[2] = (Enable == true) ? 1 : 0;

	Status = XIicPs_MasterSendPolled(Instance, data, sizeof(data), DacAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}
	return XST_SUCCESS;
}

static s32 FindDAC(XIicPs *Instance, u16 DacAddress) {
	XIicPs_EnableSlaveMonitor(Instance, DacAddress);

	u32 Index = 0, IntrStatusReg;

	while (Index < SLV_MON_LOOP_COUNT) {
		Index++;
		IntrStatusReg = XIicPs_ReadReg(Instance->Config.BaseAddress, (u32)XIICPS_ISR_OFFSET);
		if (0U != (IntrStatusReg & XIICPS_IXR_SLV_RDY_MASK)) {
			XIicPs_DisableSlaveMonitor(Instance);
			XIicPs_WriteReg(Instance->Config.BaseAddress, (u32)XIICPS_ISR_OFFSET, IntrStatusReg);
			return XST_SUCCESS;
		}
	}

	XIicPs_DisableSlaveMonitor(Instance);
	return XST_FAILURE;
}
