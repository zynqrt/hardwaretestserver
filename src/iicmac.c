/*
 * iicmac.c
 *
 *  Created on: 31 ���. 2020 �.
 *      Author: mesh
 */
#include "xparameters.h"
#include "sleep.h"
#include "xiicps.h"
#include "xil_printf.h"
#include "xplatform_info.h"
#include "iicmac.h"


#define IIC_SCLK_RATE		100000
#define SLV_MON_LOOP_COUNT 0x000FFFFF	/**< Slave Monitor Loop Count*/


static s32 MacCreateInstance(XIicPs *Instance, u16 DeviceId);
static s32 MacFindEeprom(XIicPs *Instance, u16 EepromAddress);


s32 GetMacAddress(u16 DeviceId, u16 EepromAddr, u8 *mac) {
u8 wb[16];
u8 rb[16];

	// MAC EEPROM ����� �� I2C1 (����� ??0??), ���������� � MIO12 13. ����� 1010 (0x50)
XIicPs Instance;
s32 Status;

	Status = MacCreateInstance(&Instance, DeviceId);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	Status = MacFindEeprom(&Instance, EepromAddr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	wb[0] = 0xFA;
	Status = XIicPs_MasterSendPolled(&Instance, wb, 1, EepromAddr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	while (XIicPs_BusIsBusy(&Instance));
	usleep(250000);

	Status = XIicPs_MasterRecvPolled(&Instance, rb, 6, EepromAddr);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	while (XIicPs_BusIsBusy(&Instance));

	memcpy(mac, rb, 6);
	return XST_SUCCESS;
}


static s32 MacCreateInstance(XIicPs *Instance, u16 DeviceId) {
	s32 Status;
	XIicPs_Config *ConfigPtr;

	ConfigPtr = XIicPs_LookupConfig(DeviceId);
	if (ConfigPtr == NULL) {
		return XST_FAILURE;
	}

	Status = XIicPs_CfgInitialize(Instance, ConfigPtr, ConfigPtr->BaseAddress);
	if (Status != XST_SUCCESS) {
		return XST_FAILURE;
	}

	XIicPs_SetSClk(Instance, 100000);
	return XST_SUCCESS;
}

static s32 MacFindEeprom(XIicPs *Instance, u16 EepromAddress) {

	XIicPs_EnableSlaveMonitor(Instance, EepromAddress);
	u32 Index = 0, IntrStatusReg;

	while (Index < SLV_MON_LOOP_COUNT) {
		Index++;

		IntrStatusReg = XIicPs_ReadReg(Instance->Config.BaseAddress, (u32)XIICPS_ISR_OFFSET);
		if (0U != (IntrStatusReg & XIICPS_IXR_SLV_RDY_MASK)) {
			XIicPs_DisableSlaveMonitor(Instance);
			XIicPs_WriteReg(Instance->Config.BaseAddress, (u32)XIICPS_ISR_OFFSET, IntrStatusReg);
			return XST_SUCCESS;
		}
	}

	XIicPs_DisableSlaveMonitor(Instance);
	return XST_FAILURE;
}

